# Desarrollo de videojuegos: Trabajo 1

## Introducción
En este trabajo se desarrolló un minijuego en primera persona utilizando todos los conocimientos adquiridos en clases en **Unity**.
<br><br>
## El Minijuego
El objetivo es eliminar las dianas que vayan apareciendo **de forma aleatoria** en pantalla. La cantidad de disparos necesarios para eliminar la diana depende de la dificultad en la que se encuentre el juego.
<br><br>
El juego tiene tres dificultades:
* Fácil
    * La diana requiere de 1 disparo para ser eliminada
* Medio
    * La diana requiere de 2 disparos para ser eliminada
* Difícil
    * La diana requiere de 3 disparos para ser eliminada

Una vez destruidos todos los objetivos, la escena se recargará o cambiará.
<br>
<br>
El juego tiene implementación completa de sonidos ambientales.
