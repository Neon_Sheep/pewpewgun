﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N_Movimiento_Y : MonoBehaviour
{
    public GameObject bloque;
    public float valor;

    private float d;

    void Update()
    {
        StartCoroutine(movimiento());
    }

    IEnumerator movimiento()
    {
        d = Random.Range(1, 3);

        if (d == 1)
        {
            for (int i = 0;i<3550;i++)
            {
                bloque.transform.position = new Vector3(bloque.transform.position.x, bloque.transform.position.y + valor, bloque.transform.position.z);
                if (bloque.transform.position.y <= -10.0f || bloque.transform.position.y >= 100.0f)
                {
                    bloque.transform.position = new Vector3(bloque.transform.position.x, 40.0f, bloque.transform.position.z);
                }
                yield return new WaitForSeconds(0.75f);
            }
        }
        else
        {
            for (int i = 0; i < 3550; i++)
            {
                bloque.transform.position = new Vector3(bloque.transform.position.x, bloque.transform.position.y - valor, bloque.transform.position.z);
                if (bloque.transform.position.y <= -10.0f || bloque.transform.position.y >= 100.0f)
                {
                    bloque.transform.position = new Vector3(bloque.transform.position.x, 40.0f, bloque.transform.position.z);
                }
                yield return new WaitForSeconds(0.75f);
            }
        }
    }
}
