﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N_Movimiento_X : MonoBehaviour
{
    public GameObject bloque;
    public float valor;

    private int d;

    private void Start()
    {
        d = Random.Range(1, 3);
    }

    void Update()
    {
        StartCoroutine(movimiento());
    }

    IEnumerator movimiento()
    {
        if (d == 1)
        {
            bloque.transform.position = new Vector3(bloque.transform.position.x - valor, bloque.transform.position.y, bloque.transform.position.z);
            if (bloque.transform.position.x <= -350.0f)
            {
                bloque.transform.position = new Vector3(350.0f, bloque.transform.position.y, bloque.transform.position.z);
            }
        }
        else
        {
            bloque.transform.position = new Vector3(bloque.transform.position.x + valor, bloque.transform.position.y, bloque.transform.position.z);
            if (bloque.transform.position.x >= 350.0f)
            {
                bloque.transform.position = new Vector3(-350.0f, bloque.transform.position.y, bloque.transform.position.z);
            }
        }
        yield return new WaitForSeconds(0.5f);
    }
}
