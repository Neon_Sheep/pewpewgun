﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    public int HP; 

    // Start is called before the first frame update
    void Start()
    {
        switch(Dificultad.dificultad){
            case "facil":
                HP = 1;
                break;
            case "medio":
                HP = 2;
                break;
            case "dificil":
                HP = 3;
                break; 
        }
    }
}
