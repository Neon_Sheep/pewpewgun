﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RayCast : MonoBehaviour
{
    private int m = 0;

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; 

        if(Physics.Raycast(ray, out hit, 500))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.red);

            if(Input.GetMouseButtonDown(0))
            {
                if (hit.collider.tag == "Enemigo")
                {
                    EnemyHP enemy = hit.collider.GetComponent<EnemyHP>();
                    enemy.HP -= 1; 
                    if(enemy.HP == 0)
                    {
                        Destroy(hit.collider.gameObject);
                        m++;
                    }

                    if (m==15 && SceneManager.GetActiveScene().buildIndex == 3)
                    {
                        SceneManager.LoadScene("SampleScene");
                        m = 0;
                    }

                    if (m==10 && SceneManager.GetActiveScene().buildIndex == 2)
                    {
                        SceneManager.LoadScene("SampleScene2");
                        m = 0;
                    }
                    
                    if (m>=5 && SceneManager.GetActiveScene().buildIndex == 1)
                    {
                        SceneManager.LoadScene("SampleScene1");
                        m = 0;
                    }
                    
                }

            }
        }
    }
}
