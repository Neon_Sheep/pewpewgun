﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N_Spawn : MonoBehaviour
{
    public GameObject Diana;
    private float[] pX = {-250.0f,250.0f};
    private float[] pY = {1.0f,80.0f};

    void Start()
    {
        Diana.transform.position = new Vector3(Random.Range(pX[0],pX[1]), Random.Range(pY[0],pY[1]), Diana.transform.position.z);
    }
}
